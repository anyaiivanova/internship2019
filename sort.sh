#!/bin/sh

for name in AnnaIvanova MarinaNikolaeva OlgaChernukhina
do
	for i in 1 2 3
	do
		cd "$name.$i"
		
		for bound in 100 150 200
		do
			mkdir "$bound"
			mv -f *.$bound.edf "$bound"
		done
	
		cd ..
	done
done




