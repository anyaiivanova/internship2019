import glob
import os
import pickle
from os import listdir

import mne
import numpy as np
from scipy.signal import stft


# IAF is Individual Alpha Frequency
#
# Function to calculate IAF.
def IAF(age):
    return 11.95 - 0.053 * age


# ERD is event-related desynchronization.
#
# Implementing function to calculate ERD.
# exp_value is a value of lower alpha band.
# cal_value is a value obtained during calibration.
def ERD(exp_value, cal_value):
    return 100 * (cal_value - exp_value) / cal_value


electrodes_list = ['EEG F3-Cz', 'EEG F4-Cz', 'EEG Fz-Cz', 'EEG C3-Cz',
                   'EEG C4-Cz', 'EEG P3-Cz', 'EEG P4-Cz', 'EEG Pz-Cz']

load_dir = './EEG_Export'
obj_dir = './obj_dumps'
directories = listdir(load_dir)

L1A_H_OFFSET = -2
L2A_H_OFFSET = 0
UA_H_OFFSET = 2
TH_H_OFFSET = -4

L1A_L_OFFSET = -4
L2A_L_OFFSET = -2
UA_L_OFFSET = 0
TH_L_OFFSET = -6


# raw_calib is a raw data recorded with opened eyes during calibration
# raw_data is a raw data recorded during experiment
def processData(raw_calib, raw_data):
    # Sample frequency of recorded data
    sfreq = raw_data.info['sfreq']

    # Channels for eyes data
    eyes = raw_calib.copy().pick_channels(ch_names=electrodes_list)
    # Channels for experiment data
    experiment = raw_data.copy().pick_channels(ch_names=electrodes_list)

    # Filtering AC line noise with notch filter

    # eyes_filtered_data = mne.filter.notch_filter(x=eyes.get_data(), Fs=sfreq, freqs=[50, 100])
    # experiment_filtered_data = mne.filter.notch_filter(x=experiment.get_data(), Fs=sfreq, freqs=[50, 100])
    eyes_filtered_data = eyes.get_data()
    experiment_filtered_data = experiment.get_data()

    # Preparing data for plotting
    eyes_filtered = mne.io.RawArray(data=eyes_filtered_data,
                                    info=mne.create_info(ch_names=electrodes_list, sfreq=sfreq))
    experiment_filtered = mne.io.RawArray(data=experiment_filtered_data,
                                          info=mne.create_info(ch_names=electrodes_list, sfreq=sfreq))

    # Calculate IAF for age of 20.
    IAF_p = IAF(20)

    # Getting L1A (lower alpha 1), L2A (lower alpha 2), UA (upper alpha), Theta waves
    # from calibration data using FIR filtering.
    # Also we take mean signal from all channels.
    calib_channels = {
        'L1A': mne.filter.filter_data(data=np.mean(eyes_filtered.get_data(), axis=0), l_freq=IAF_p + L1A_L_OFFSET,
                                      h_freq=IAF_p + L1A_H_OFFSET, sfreq=sfreq, method="fir"),
        'L2A': mne.filter.filter_data(data=np.mean(eyes_filtered.get_data(), axis=0), l_freq=IAF_p + L2A_L_OFFSET,
                                      h_freq=IAF_p + L2A_H_OFFSET, sfreq=sfreq, method="fir"),
        'UA': mne.filter.filter_data(data=np.mean(eyes_filtered.get_data(), axis=0), l_freq=IAF_p + UA_L_OFFSET,
                                     h_freq=IAF_p + UA_H_OFFSET, sfreq=sfreq, method="fir"),
        'Th': mne.filter.filter_data(data=np.mean(eyes_filtered.get_data(), axis=0), l_freq=IAF_p + TH_L_OFFSET,
                                     h_freq=IAF_p + TH_H_OFFSET, sfreq=sfreq, method="fir")
    }

    # Getting L1A, L2A, UA, Theta waves from experiment data using FIR filtering.
    # Also we take mean signal from all channels
    data_channels = {
        'L1A': mne.filter.filter_data(data=np.mean(experiment_filtered.get_data(), axis=0),
                                      l_freq=IAF_p + L1A_L_OFFSET, h_freq=IAF_p + L1A_H_OFFSET, sfreq=sfreq,
                                      method="fir"),
        'L2A': mne.filter.filter_data(data=np.mean(experiment_filtered.get_data(), axis=0),
                                      l_freq=IAF_p + L2A_L_OFFSET, h_freq=IAF_p + L2A_H_OFFSET, sfreq=sfreq,
                                      method="fir"),
        'UA': mne.filter.filter_data(data=np.mean(experiment_filtered.get_data(), axis=0),
                                     l_freq=IAF_p + UA_L_OFFSET, h_freq=IAF_p + UA_H_OFFSET, sfreq=sfreq, method="fir"),
        'Th': mne.filter.filter_data(data=np.mean(experiment_filtered.get_data(), axis=0),
                                     l_freq=IAF_p + TH_L_OFFSET, h_freq=IAF_p + TH_H_OFFSET, sfreq=sfreq, method="fir")
    }

    # Calculating calibration values. Consider mean value for each channel. Values are given in microvolts
    calibration_values = {}

    for band in calib_channels:
        calibration_values[band] = np.mean(calib_channels[band], axis=0) * np.power(10, 6)

    # Performing STFT transform on experiment data for each sub-band. Window size is given in samples.
    # 2 seconds of window.
    window = sfreq * 2
    fft = {}

    for band in data_channels:
        fft[band] = stft(x=data_channels[band], fs=sfreq, window=('kaiser', window), nperseg=1000)

    erd = np.vectorize(ERD)

    # Final ERD results of the experiment.
    # Contains ERD values for each band.
    erd_mean = {}

    # Add all of the bands to the results.
    for band in fft:
        erd_result = erd(fft[band][2], calibration_values[band])
        erd_mean[band] = np.mean(erd_result, axis=0)

    return erd_mean


def processSession(type: str, dir: str):
    print('Processing session "' + dir + '"')

    calib_e_o = glob.glob(dir + "/*." + type + ".*opened*.edf")
    data_files = glob.glob(dir + "/*." + type + ".*data*.edf")

    print('Calib files:')
    for file in calib_e_o:
        print('    ' + file)

    # Raw data for calibration data and experiment data
    raw_calib = mne.io.read_raw_edf(calib_e_o[0], preload=True)

    for data_file in data_files:
        simple_file_name = os.path.basename(os.path.normpath(data_file))

        raw1_data = mne.io.read_raw_edf(data_file, preload=True)
        erd_mean = processData(raw_calib, raw1_data)

        erd_mean['name'] = simple_file_name

        res_dir = dir.replace('EEG_Export', 'obj_dumps')
        # Create parent directories
        try:
            os.makedirs(res_dir)
        except Exception:
            pass
        # Dumping erd_mean of experiment
        pickle.dump(erd_mean, open(res_dir + '/' + simple_file_name + ".pkl", 'wb'))


# Directory is in format NAME.EXPERIMENT_NUMBER
for directory in directories:
    # Fix macOS problems
    if directory == '.DS_Store':
        continue

    bands = listdir(load_dir + '/' + directory)

    for band in bands:
        # Fix macOS problems
        if band == '.DS_Store':
            continue

        final_path = load_dir + '/' + directory + '/' + band
        processSession('Control', final_path)
        processSession('Extra', final_path)
