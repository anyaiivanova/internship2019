import glob
import os
import pickle
from os import listdir

import matplotlib.pyplot as plt
import seaborn as sns

# Directory which contains dumped objects from previous stage
obj_dir = './obj_dumps'
# The band that we analyze (one of L1A, L2A, UA and TH).
#   - Lower alpha is responsible for attention (what we research)
#   - Upper alpha is responsible for semantic memory performance
freq_band = 'L2A'

for session in listdir(obj_dir):
    # Fix macOS problems
    if session == '.DS_Store':
        continue

    # Make directory for figures for current session if it doesn't exist
    try:
        os.makedirs('figs/' + session)
    except Exception as e:
        pass

    # To through each bound (100, 150 and 200) of the current session
    for bound in listdir(obj_dir + '/' + session):
        # Fix macOS problems
        if session == '.DS_Store':
            continue

        # Clear plot state and name it
        plt.clf()
        plt.title(session + '/' + bound + '/' + freq_band)

        # Filter all control and extra files
        control_files = glob.glob(obj_dir + '/' + session + '/' + bound + "/*Control*.edf.pkl")
        extra_files = glob.glob(obj_dir + '/' + session + '/' + bound + "/*Extra*.edf.pkl")

        # Init i as a label counter
        i = 1
        # Add all control files' data to the plot
        for control_file in control_files:
            with open(control_file, 'rb') as f:
                result = pickle.load(f)
                sns.kdeplot(result[freq_band], label='No piano ' + str(i), color='#0000ff40')
                i += 1

        # Init i as a label counter
        i = 1
        # Add all extra files' data to the plot
        for extra_file in extra_files:
            with open(extra_file, 'rb') as f:
                result = pickle.load(f)
                sns.kdeplot(result[freq_band], label='Piano ' + str(i), color='#00ff0040')
                i += 1

        # Save current figure to the file
        sns.mpl.pyplot.savefig('figs/' + session + '/' + bound + '.png', dpi=192)