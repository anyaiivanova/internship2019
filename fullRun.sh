#!/bin/sh

# Activate VirtualEnv
source venv/bin/activate

# Remove macOS's junk files
rm -rf ./EEG_Export/.DS_Store
rm -rf ./obj_dumps/.DS_Store

# Run converter
python3 main.py

# Run visualizer
python3 analyze.py



